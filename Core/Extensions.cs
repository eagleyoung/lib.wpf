﻿using System;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Threading;

namespace Lib.WPF
{
    public static class Extensions
    {
        /// <summary>
        /// Запустить задачу в UI потоке
        /// </summary>
        public static void UIInvoke(this Action action)
        {
            Application.Current.Dispatcher.Invoke(action);
        }

        /// <summary>
        /// Запустить задачу в UI потоке асинхронно
        /// </summary>
        public static DispatcherOperation UIBeginInvoke(this Action action)
        {
            return Application.Current.Dispatcher.BeginInvoke(action);
        }

        /// <summary>
        /// Выполнить задачу в окне
        /// </summary>
        public static void ForWindowFromTemplate(this object templateFrameworkElement, Action<Window> action)
        {
            if (((FrameworkElement)templateFrameworkElement).TemplatedParent is Window window) action(window);
        }

        /// <summary>
        /// Получить указатель на окно
        /// </summary>
        public static IntPtr GetWindowHandle(this Window window)
        {
            WindowInteropHelper helper = new WindowInteropHelper(window);
            return helper.Handle;
        }
    }
}
