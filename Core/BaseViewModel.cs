﻿using System;
using System.Threading.Tasks;

namespace Lib.WPF
{
    /// <summary>
    /// Стандартная модель представления окна
    /// </summary>
    public class BaseViewModel : NotifyObject
    {
        string windowLockText;
        /// <summary>
        /// Сообщение при блокировке окна
        /// </summary>
        public string WindowLockText
        {
            get
            {
                return windowLockText;
            }
            set
            {
                if(windowLockText != value)
                {
                    windowLockText = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Событие при запросе закрытия окна из модели представления
        /// </summary>
        public event Action RequestClose = delegate { };

        /// <summary>
        /// Запросить закрытие окна
        /// </summary>
        protected void OnRequestClose() => RequestClose();

        /// <summary>
        /// Окно может быть закрыто?
        /// </summary>
        public virtual bool CloseCheck() => true;

        /// <summary>
        /// Окно было закрыто
        /// </summary>
        public virtual void OnClosed() { }

        /// <summary>
        /// Окно было инициализировано
        /// </summary>
        public virtual void OnInitialized() {
            Task.Run(OnInitializedAsync);
        }

        protected virtual Task OnInitializedAsync() {
            return null;
        }

        string searchString;
        /// <summary>
        /// Строка поиска в окне
        /// </summary>
        public string SearchString
        {
            get
            {
                return searchString;
            }
            set
            {
                if(searchString != value)
                {
                    searchString = value;
                    OnPropertyChanged();
                    Search(searchString);
                }
            }
        }

        /// <summary>
        /// Осуществить поиск в окне
        /// </summary>
        protected virtual void Search(string text) { }      
    }
}
