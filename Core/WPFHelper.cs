﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System.Windows;
using System.Windows.Media;

namespace Lib.WPF
{
    /// <summary>
    /// Вспомогательные функции
    /// </summary>
    public static class WPFHelper
    {
        /// <summary>
        /// Найти дочерний элемент определенного типа.
        /// При указании имени ищет элемент определенного типа с определенным именем.
        /// </summary>
        public static T FindChild<T>(DependencyObject parent, string childName = null)
            where T : DependencyObject
        {
            if (parent == null) return null;

            T found = null;

            int childCount = VisualTreeHelper.GetChildrenCount(parent);
            for(int i = 0; i < childCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                if (child is T childT)
                {
                    if (childName != null)
                    {
                        var name = (child as FrameworkElement).Name;
                        if (name == childName)
                        {
                            found = childT;
                            break;
                        }
                        else
                        {
                            found = FindChild<T>(child, childName);
                            if (found != null) break;
                        }
                    }
                    else
                    {
                        found = childT;
                        break;
                    }
                }
                else
                {
                    found = FindChild<T>(child, childName);
                    if (found != null) break;
                }
            }

            return found;
        }

        /// <summary>
        /// Получить строку для сохранения файла
        /// </summary>
        public static string GetFileSavePath(string fileName, string fileExtension, string filter)
        {
            CommonSaveFileDialog dlg = new CommonSaveFileDialog
            {
                DefaultFileName = fileName,
                DefaultExtension = fileExtension
            };
            dlg.Filters.Add(new CommonFileDialogFilter(filter, filter));
            
            var result = dlg.ShowDialog();

            if (result == CommonFileDialogResult.Ok)
            {
                return dlg.FileName;
            }

            return null;
        }

        /// <summary>
        /// Получить строку для открытия файла
        /// </summary>
        public static string GetFileOpenPath(string fileExtension, string filter)
        {
            CommonOpenFileDialog dlg = new CommonOpenFileDialog() {
                DefaultExtension = fileExtension
            };
            dlg.Filters.Add(new CommonFileDialogFilter(filter, filter));
            
            var result = dlg.ShowDialog();

            if (result == CommonFileDialogResult.Ok)
            {
                return dlg.FileName;
            }

            return null;
        }

        /// <summary>
        /// Получить строку до директории
        /// </summary>
        public static string GetFolderPath(string title, string initial = null)
        {
            CommonOpenFileDialog dlg = new CommonOpenFileDialog() {
                IsFolderPicker = true
            };

            if (title != null) dlg.Title = title;
            if(initial != null) dlg.InitialDirectory = initial;

            var result = dlg.ShowDialog();

            if(result == CommonFileDialogResult.Ok)
            {
                return dlg.FileName;
            }

            return initial;
        }
    }
}
