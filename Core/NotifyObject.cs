﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Lib.WPF
{
    /// <summary>
    /// Объект с возможностью рассылки информации об изменении
    /// </summary>
    public class NotifyObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Параметр объекта был изменен
        /// </summary>
        protected virtual void OnPropertyChanged([CallerMemberName] string key = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(key));
        }
    }
}
