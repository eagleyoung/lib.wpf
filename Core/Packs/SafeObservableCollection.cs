﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Data;

namespace Lib.WPF
{
    /// <summary>
    /// Потокобезопасная отслеживаемая коллекция
    /// </summary>
    public class SafeObservableCollection<T> : ObservableCollection<T>
    {
        /// <summary>
        /// Объект для синхронизации взаимодействия с коллекцией
        /// </summary>
        protected object locker = new object();

        public SafeObservableCollection()
        {
            BindingOperations.EnableCollectionSynchronization(this, locker);
        }

        protected override void ClearItems()
        {
            lock (locker)
            {
                base.ClearItems();
            }            
        }

        protected override void InsertItem(int index, T item)
        {
            lock (locker)
            {
                base.InsertItem(index, item);
            }
        }

        protected override void MoveItem(int oldIndex, int newIndex)
        {
            lock (locker)
            {
                base.MoveItem(oldIndex, newIndex);
            }
        }

        protected override void RemoveItem(int index)
        {
            lock (locker)
            {
                base.RemoveItem(index);
            }
        }

        protected override void SetItem(int index, T item)
        {
            lock (locker)
            {
                base.SetItem(index, item);
            }            
        }

        public virtual void AddRange(IEnumerable<T> items)
        {
            lock (locker)
            {
                CheckReentrancy();
                foreach(var item in items)
                {
                    Items.Add(item);
                }

                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                OnPropertyChanged(new PropertyChangedEventArgs("Count"));
                OnPropertyChanged(new PropertyChangedEventArgs("Item[]"));
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string key = null)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(key));
        }
    }

    public static class SafeObservableCollection
    {
        /// <summary>
        /// Создать новую коллекцию
        /// </summary>
        public static SafeObservableCollection<T> Create<T>()
        {
            SafeObservableCollection<T> output = null;
            new Action(delegate {
                output = new SafeObservableCollection<T>();
            }).UIInvoke();
            return output;
        }
    }
}
