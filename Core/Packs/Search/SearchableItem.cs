﻿using System.Collections.Generic;

namespace Lib.WPF
{
    /// <summary>
    /// Объект с возможностью поиска
    /// </summary>
    public abstract class SearchableItem : NotifyObject
    {
        protected override void OnPropertyChanged(string key = null)
        {
            base.OnPropertyChanged(key = null);
            search.Clear();
        }

        protected SearchData search;

        public SearchableItem()
        {
            search = new SearchData(()=> {
                return GenerateSearch();
            });
        }

        /// <summary>
        /// Поисковые данные содержат набор строк?
        /// </summary>
        public bool Contains(List<string> data)
        {
            return search.Contains(data);
        }

        /// <summary>
        /// Сгенерированный набор поисковых данных
        /// </summary>
        public SearchDataPack GeneratedSearch()
        {
            return GenerateSearch();
        }

        protected abstract SearchDataPack GenerateSearch();
    }
}
