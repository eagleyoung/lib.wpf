﻿using System;
using System.Collections.Generic;

namespace Lib.WPF
{
    /// <summary>
    /// Данные о поиске объекта
    /// </summary>
    public class SearchData
    {
        string generated;

        /// <summary>
        /// Очистить сгенерированные данные
        /// </summary>
        public void Clear()
        {
            generated = null;
        }

        Func<SearchDataPack> action;

        public SearchData(Func<SearchDataPack> _action)
        {
            action = _action;
        }

        /// <summary>
        /// Объект содержит данные?
        /// </summary>
        public bool Contains(List<string> data)
        {
            if (generated == null) {
                generated = action.Invoke().Get();
                if (generated == null) return false;
            }
            foreach (var item in data)
            {
                if (!generated.Contains(item)) return false;
            }

            return true;
        }
    }
}
