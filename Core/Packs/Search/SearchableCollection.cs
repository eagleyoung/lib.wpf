﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Lib.WPF
{
    /// <summary>
    /// Потокобезопасная отслеживаемая коллекция с возможностью сортировки, фильтрации и поиска
    /// </summary>
    public class SearchableCollection<T> : FilteredCollection<T> where T : SearchableItem
    {
        List<string> ParseSearch(string str)
        {
            if (str == null) return null;
            List<string> generated = new List<string>();
            foreach (var item in str.ToUpper().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var trim = item.Trim();
                if (trim.Length > 0)
                {
                    generated.Add(trim);
                }
            }
            return generated;
        }

        List<string> searchData;
        /// <summary>
        /// Осуществить поиск по набору
        /// </summary>
        public void Search(string _searchString)
        {
            searchData = ParseSearch(_searchString);
            RefreshFilter();
        }

        protected override bool PerformFilter(T item)
        {
            var filter = base.PerformFilter(item);

            if (!filter) return false;
            if (searchData == null) return true;

            return item.Contains(searchData);
        }        
    }

    public static class SearchableCollection
    {
        /// <summary>
        /// Создать новую коллекцию
        /// </summary>
        public static SearchableCollection<T> Create<T>()
            where T : SearchableItem
        {
            SearchableCollection<T> output = null;
            new Action(delegate
            {
                output = new SearchableCollection<T>();
            }).UIInvoke();
            return output;
        }   
        
        /// <summary>
        /// Создать новую коллекцию с указанным компаратором
        /// </summary>
        public static SearchableCollection<T> Create<T>(IComparer comparer)
            where T : SearchableItem
        {
            var col = Create<T>();
            col.SetComparer(comparer);
            return col;
        }     
    }
}
