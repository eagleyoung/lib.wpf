﻿using System.Text;

namespace Lib.WPF
{
    /// <summary>
    /// Объект для передачи данных о поиске
    /// </summary>
    public class SearchDataPack
    {
        StringBuilder builder = new StringBuilder();

        /// <summary>
        /// Получить сгенерированную строку поиска
        /// </summary>
        public string Get()
        {
            return builder.ToString();
        }
        
        /// <summary>
        /// Добавить строку
        /// </summary>
        public void Add(string value)
        {
            if (value == null) return;
            builder.AppendFormat("{0} ", value.ToUpper());
        }

        /// <summary>
        /// Добавить данные другого объекта
        /// </summary>
        public void Add(SearchableItem item)
        {
            var data = item.GeneratedSearch();
            Add(data.Get());          
        }
    }
}
