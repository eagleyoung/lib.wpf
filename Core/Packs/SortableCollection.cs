﻿using System;
using System.Collections;
using System.Windows.Data;

namespace Lib.WPF
{
    /// <summary>
    /// Потокобезопасная отслеживаемая коллекция с возможностью сортировки
    /// </summary>
    public class SortableCollection<T> : SafeObservableCollection<T>
    {
        /// <summary>
        /// Представление коллекции
        /// </summary>
        public ListCollectionView View { get; }

        public SortableCollection()
        {
            View = new ListCollectionView(this);
        }

        /// <summary>
        /// Установить сортировщик коллекции
        /// </summary>
        public void SetComparer(IComparer comparer)
        {
            View.CustomSort = comparer;
        }
    }

    public static class SortableCollection
    {
        /// <summary>
        /// Создать новую коллекцию
        /// </summary>
        public static SortableCollection<T> Create<T>()
        {
            SortableCollection<T> output = null;
            new Action(delegate {
                output = new SortableCollection<T>();
            }).UIInvoke();
            return output;
        }

        /// <summary>
        /// Создать новую коллекцию с указанным компаратором
        /// </summary>
        public static SortableCollection<T> Create<T>(IComparer comparer)
        {
            var col = Create<T>();
            col.SetComparer(comparer);
            return col;
        }
    }
}
