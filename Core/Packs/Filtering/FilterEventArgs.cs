﻿using System;

namespace Lib.WPF
{
    public class FilterEventArgs : EventArgs
    {
        /// <summary>
        /// Элемент должен быть показан?
        /// </summary>
        public bool Enabled { get; set; } = true;
    }
}
