﻿using System;
using System.Collections;

namespace Lib.WPF
{
    /// <summary>
    /// Потокобезопасная отслеживаемая коллекция с возможностью сортировки и фильтрации
    /// </summary>
    public class FilteredCollection<T> : SortableCollection<T>
    {
        public FilteredCollection()
        {            
            View.Filter = i => PerformFilter((T)i);
        }

        protected virtual bool PerformFilter(T item)
        {
            if (item == null) return false;
            return OnFiltering(item).Enabled;
        }

        public event EventHandler<FilterEventArgs> Filtering;
        FilterEventArgs OnFiltering(T item)
        {
            var handler = Filtering;
            var args = new FilterEventArgs();
            if (handler != null)
            {
                handler(item, args);
            }
            return args;
        }

        /// <summary>
        /// Добавить новый фильтр
        /// </summary>
        public void AttachFilter(FilterItem _item)
        {
            Filtering += _item.Args;
            _item.StateChanged += ItemStateChanged;
            RefreshFilter();
        }

        private void ItemStateChanged()
        {
            RefreshFilter();
        }

        /// <summary>
        /// Обновить фильтрацию коллекции
        /// </summary>
        public void RefreshFilter()
        {
            View.Refresh();
        }

        /// <summary>
        /// Убрать фильтр
        /// </summary>
        public void RemoveFilter(FilterItem _item)
        {
            Filtering -= _item.Args;
            _item.StateChanged -= ItemStateChanged;
            RefreshFilter();
        }
    }

    public static class FilteredCollection
    {
        /// <summary>
        /// Создать новую коллекцию
        /// </summary>
        public static FilteredCollection<T> Create<T>()
        {
            FilteredCollection<T> output = null;
            new Action(delegate {
                output = new FilteredCollection<T>();
            }).UIInvoke();
            return output;
        }

        /// <summary>
        /// Создать новую коллекцию с указанным компаратором
        /// </summary>
        public static FilteredCollection<T> Create<T>(IComparer comparer)
        {
            var col = Create<T>();
            col.SetComparer(comparer);
            return col;
        }
    }
}
