﻿using System;

namespace Lib.WPF
{
    /// <summary>
    /// Фильтр
    /// </summary>
    public class FilterItem
    {
        public EventHandler<FilterEventArgs> Args { get; }

        public FilterItem(EventHandler<FilterEventArgs> _args, bool _enabled = true)
        {
            Args = _args;
            enabled = _enabled;
        }

        bool enabled = true;
        /// <summary>
        /// Фильтр работает?
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                if(enabled != value)
                {
                    enabled = value;
                    StateChanged();
                }
            }
        }

        /// <summary>
        /// Состояние элемента изменилось
        /// </summary>
        public event Action StateChanged = delegate { };
    }
}
