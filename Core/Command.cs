﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace Lib.WPF
{
    /// <summary>
    /// Стандартная команда для реагирования на пользовательский ввод
    /// </summary>
    public class Command : ICommand, INotifyPropertyChanged
    {
        Action action = null;
        Action<object> parameterizedAction = null;

        public Command(Action _action, bool _canExecute = true)
        {
            action = _action;
            canBeExecuted = _canExecute;
        }

        public Command(Action<object> _parameterizedAction, bool _canExecute = true)
        {
            parameterizedAction = _parameterizedAction;
            canBeExecuted = _canExecute;
        }  
        
        public Command() { }
        
        /// <summary>
        /// Установить задачу для команды
        /// </summary>
        public void SetAction(Action _action)
        {
            action = _action;
        }  
        
        bool canBeExecuted = true;
        /// <summary>
        /// Команда может быть выполнена?
        /// </summary>
        public bool CanBeExecuted
        {
            get {
                return canBeExecuted;
            }
            set
            {
                if (canBeExecuted != value)
                {
                    canBeExecuted = value;                    
                    CanExecuteChanged?.Invoke(this, EventArgs.Empty);
                    OnPropertyChanged();
                }
            }
        }

        public bool CanExecute(object parameter)
        {
            return CanBeExecuted;
        }

        public void Execute(object parameter)
        {
            if(action != null)
            {
                action.Invoke();
            }
            else if(parameterizedAction != null)
            {
                parameterizedAction.Invoke(parameter);
            }
        }

        public event EventHandler CanExecuteChanged;

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string key = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(key));
        }
    }
}
