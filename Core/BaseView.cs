﻿using Lib.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Lib.WPF
{
    /// <summary>
    /// Стандартное представление окна
    /// </summary>
    public class BaseView : Window
    {
        static List<BaseView> allViews = new List<BaseView>();

        /// <summary>
        /// Скрыть все представления
        /// </summary>
        /// <param name="except">Исключаемый объект</param>
        public void HideAll(BaseView except = null)
        {
            foreach(var view in allViews)
            {
                if (view != except) view.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Показать все представления
        /// </summary>
        public void ShowAll()
        {
            foreach(var view in allViews)
            {
                view.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Закрыть все представления
        /// </summary>
        /// <param name="except">Исключаемый объект</param>
        public void CloseAll(BaseView except = null)
        {
            for(int i = allViews.Count - 1; i >= 0; i--)
            {
                var view = allViews[i];
                if(view != except)
                {
                    view.Close();
                }
            }
        }

        /// <summary>
        /// Модель представления
        /// </summary>
        protected BaseViewModel Vm
        {
            get
            {
                return DataContext as BaseViewModel;
            }
        }

        /// <summary>
        /// Зарегистрировать модель данных
        /// </summary>
        public void RegisterModel(BaseViewModel model = null) {
            if (model != null) DataContext = model;
            if (Vm != null)
            {
                Vm.RequestClose += RequestCloseReceived;
                Vm.OnInitialized();
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            RegisterModel();
            Output.Traced += OutputTraced;

            allViews.Add(this);
        }

        void OutputTraced(OutputData obj)
        {
            new Action(delegate {
                if (IsActive)
                {
                    OutString = obj.Text;
                    OutVariant = obj.Variant;
                    OutAdditional = obj.Additional;
                }
            }).UIInvoke();            
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (Vm != null)
            {
                Vm.RequestClose -= RequestCloseReceived;
                Vm.OnClosed();
            }
            Output.Traced -= OutputTraced;

            allViews.Remove(this);
        }

        void RequestCloseReceived()
        {
            new Action(delegate {
                Close();
            }).UIInvoke();            
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if(Vm != null)
            {
                if (!Vm.CloseCheck()) e.Cancel = true;
            }            
        }

        public static readonly DependencyProperty CanTraceOutputProperty = DependencyProperty.Register("CanTraceOutput", typeof(bool), typeof(BaseView), new UIPropertyMetadata(true));
        public bool CanTraceOutput
        {
            get
            {
                return (bool)GetValue(CanTraceOutputProperty);
            }
            set
            {
                SetValue(CanTraceOutputProperty, value);
            }
        }

        public static readonly DependencyProperty CloseOnEscProperty = DependencyProperty.Register("CloseOnEsc", typeof(bool), typeof(BaseView), new UIPropertyMetadata(false));
        public bool CloseOnEsc
        {
            get
            {
                return (bool)GetValue(CloseOnEscProperty);
            }
            set
            {
                SetValue(CloseOnEscProperty, value);
            }
        }

        public static readonly DependencyProperty SearchAvailableProperty = DependencyProperty.Register("SearchAvailable", typeof(bool), typeof(BaseView), new UIPropertyMetadata(false));
        public bool SearchAvailable
        {
            get
            {
                return (bool)GetValue(SearchAvailableProperty);
            }
            set
            {
                SetValue(SearchAvailableProperty, value);
            }
        }

        public static readonly DependencyProperty ResizeAvailableProperty = DependencyProperty.Register("ResizeAvailable", typeof(bool), typeof(BaseView), new UIPropertyMetadata(true));
        public bool ResizeAvailable
        {
            get
            {
                return (bool)GetValue(ResizeAvailableProperty);
            }
            set
            {
                SetValue(ResizeAvailableProperty, value);
            }
        }        

        public static readonly DependencyProperty ProgressProperty = DependencyProperty.Register("Progress", typeof(double), typeof(BaseView), new UIPropertyMetadata(-1d));
        public double Progress
        {
            get
            {
                return (double)GetValue(ProgressProperty);
            }
            set
            {
                SetValue(ProgressProperty, value);
            }
        }

        public static readonly DependencyProperty SettingsProperty = DependencyProperty.Register("Settings", typeof(object), typeof(BaseView), new UIPropertyMetadata(null));
        public object Settings
        {
            get
            {
                return GetValue(SettingsProperty);
            }
            set
            {
                SetValue(SettingsProperty, value);
            }
        }

        public static readonly DependencyProperty OutStringProperty = DependencyProperty.Register("OutString", typeof(string), typeof(BaseView), new UIPropertyMetadata(null));
        public string OutString
        {
            get
            {
                return (string)GetValue(OutStringProperty);
            }
            set
            {
                SetValue(OutStringProperty, value);
            }
        }

        public static readonly DependencyProperty OutVariantProperty = DependencyProperty.Register("OutVariant", typeof(OutputData.Variants), typeof(BaseView), new UIPropertyMetadata(OutputData.Variants.Default));
        public OutputData.Variants OutVariant
        {
            get
            {
                return (OutputData.Variants)GetValue(OutVariantProperty);
            }
            set
            {
                SetValue(OutVariantProperty, value);
            }
        }

        public static readonly DependencyProperty OutAdditionalProperty = DependencyProperty.Register("OutAdditional", typeof(string), typeof(BaseView), new UIPropertyMetadata(null));
        public string OutAdditional
        {
            get
            {
                return (string)GetValue(OutAdditionalProperty);
            }
            set
            {
                SetValue(OutAdditionalProperty, value);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.Key == Key.Escape) {
                if (CloseOnEsc) Close();
            }
        }

        protected override void OnStateChanged(EventArgs e)
        {
            base.OnStateChanged(e);
            CheckBorders();
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            CheckBorders();
        }

        void CheckBorders()
        {
            var handle = this.GetWindowHandle();
            var container = (Grid)Template.FindName("PART_Container", this);
            if (container == null) return;
            if (WindowState == WindowState.Maximized)
            {
                var screen = System.Windows.Forms.Screen.FromHandle(handle);
                if (screen.Primary)
                {
                    container.Margin = new Thickness(
                        SystemParameters.WorkArea.Left + 6,
                        SystemParameters.WorkArea.Top + 6,
                        (SystemParameters.PrimaryScreenWidth - SystemParameters.WorkArea.Right) + 6,
                        (SystemParameters.PrimaryScreenHeight - SystemParameters.WorkArea.Bottom) + 4);
                }
                else
                {
                    container.Margin = new Thickness(6, 6, 6, 4);
                }
            }
            else
            {
                container.Margin = new Thickness();
            }
        }
    }
}
