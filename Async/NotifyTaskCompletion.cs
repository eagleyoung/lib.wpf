﻿using System.Threading.Tasks;

namespace Lib.WPF.Async
{
    public sealed class NotifyTaskCompletion<TResult> : NotifyObject
    {
        Task<TResult> task;        

        /// <summary>
        /// Проверка выполнения задачи
        /// </summary>
        public Task TaskCompletion { get;  }

        public NotifyTaskCompletion(Task<TResult> _task)
        {
            task = _task;
            TaskCompletion = WatchTaskAsync(task);
        }

        /// <summary>
        /// Результат задачи
        /// </summary>
        public TResult Result
        {
            get
            {
                return (task.Status == TaskStatus.RanToCompletion) ?
                    task.Result : default(TResult);
            }
        }

        bool isExecuting;
        /// <summary>
        /// Команда выполняется?
        /// </summary>
        public bool IsExecuting
        {
            get
            {
                return isExecuting;
            }
            set
            {
                isExecuting = value;
                OnPropertyChanged();
            }
        }

        async Task WatchTaskAsync(Task task)
        {
            UpdateState();
            try
            {
                IsExecuting = true;
                await task;                
            }
            catch
            { }
            IsExecuting = false;
            UpdateState();
        }

        void UpdateState()
        {
            OnPropertyChanged("Result");
        }
    }
}

