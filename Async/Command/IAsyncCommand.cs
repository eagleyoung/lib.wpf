﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace Lib.WPF.Async
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
    }
}

