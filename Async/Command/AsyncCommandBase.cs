﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Lib.WPF.Async
{
    public abstract class AsyncCommandBase : IAsyncCommand, INotifyPropertyChanged
    {
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute(parameter);
        }

        public abstract Task ExecuteAsync(object parameter);

        public async void Execute(object parameter)
        {
            await ExecuteAsync(parameter);
        }

        public event EventHandler CanExecuteChanged;

        bool canBeExecuted = true;
        /// <summary>
        /// Команда может быть выполнена?
        /// </summary>
        public bool CanBeExecuted
        {
            get
            {
                return canBeExecuted;
            }
            set
            {
                if (canBeExecuted != value)
                {
                    canBeExecuted = value;
                    CanExecuteChanged?.Invoke(this, EventArgs.Empty);
                    OnPropertyChanged();
                }
            }
        }

        public bool CanExecute(object parameter)
        {
            return CanBeExecuted;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string key = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(key));
        }
    }
}
