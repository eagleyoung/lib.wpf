﻿using System.Threading;

namespace Lib.WPF.Async
{
    /// <summary>
    /// Команда для отмены асинхронной команды
    /// </summary>
    public class CancelAsyncCommand : Command
    {
        void RequestCancel()
        {
            CanBeExecuted = false;
            if(source != null)
            {
                source.Cancel();
            }            
        }

        /// <summary>
        /// Объект для запроса остановки задачи
        /// </summary>
        public CancellationToken Token {
            get
            {
                return source.Token;
            }
        }

        CancellationTokenSource source;
        public CancelAsyncCommand() {

            Reload();
            SetAction(RequestCancel);
        }

        /// <summary>
        /// Перезагрузить команду
        /// </summary>
        public void Reload()
        {
            CanBeExecuted = true;
            source = new CancellationTokenSource();
        }
    }
}
