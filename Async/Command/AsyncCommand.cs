﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lib.WPF.Async
{
    /// <summary>
    /// Асинхронная команда
    /// </summary>
    public class AsyncCommand<TResult> : AsyncCommandBase
    {
        Func<object, CancellationToken, Task<TResult>> Command { get; }
        public CancelAsyncCommand CancelCommand { get; }

        NotifyTaskCompletion<TResult> execution;
        public NotifyTaskCompletion<TResult> Execution {
            get
            {
                return execution;
            }
            set
            {
                if(execution != value)
                {
                    execution = value;
                    OnPropertyChanged();
                }
            }
        }

        public override async Task ExecuteAsync(object parameter)
        {
            CanBeExecuted = false;
            CancelCommand.Reload();
            CancelCommand.CanBeExecuted = true;
            Execution = new NotifyTaskCompletion<TResult>(Command(parameter, CancelCommand.Token));
            await Execution.TaskCompletion;
            CancelCommand.CanBeExecuted = false;
            CanBeExecuted = true;
        }

        public AsyncCommand(Func<object, CancellationToken, Task<TResult>> _command)
        {
            Command = _command;
            CancelCommand = new CancelAsyncCommand
            {
                CanBeExecuted = false
            };
        }        
    }

    public static class AsyncCommand
    {
        public static AsyncCommand<object> Create(Func<object, Task> command)
        {
            return new AsyncCommand<object>(async (parameter, token) => { await command(parameter); return null; });
        }

        public static AsyncCommand<TResult> Create<TResult>(Func<object, Task<TResult>> command)
        {
            return new AsyncCommand<TResult>((parameter, token) => command(parameter));
        }

        public static AsyncCommand<object> Create(Func<object, CancellationToken, Task> command)
        {
            return new AsyncCommand<object>(async (parameter, token) => { await command(parameter, token); return null; });
        }

        public static AsyncCommand<TResult> Create<TResult>(Func<object, CancellationToken, Task<TResult>> command)
        {
            return new AsyncCommand<TResult>(command);
        }
    }
}
