﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace Lib.WPF.Converters
{
    public class EnumerateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new List<object> { value };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
