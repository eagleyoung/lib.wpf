﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Lib.WPF.Converters
{
    public class ReverseStringLengthToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = (string)value;
            return str != null && str.Length > 0 ? false : true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
