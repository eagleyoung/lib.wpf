﻿using System.Windows;
using System.Windows.Controls;

namespace Lib.WPF.Controls
{
    /// <summary>
    /// Логика взаимодействия для HeaderTextBox.xaml
    /// </summary>
    public partial class HeaderTextBox : UserControl
    {
        public HeaderTextBox()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty HeaderProperty = DependencyProperty.Register("Header", typeof(string), typeof(HeaderTextBox), new UIPropertyMetadata(null));
        public string Header
        {
            get
            {
                return (string)GetValue(HeaderProperty);
            }
            set
            {
                SetValue(HeaderProperty, value);
            }
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(HeaderTextBox), new FrameworkPropertyMetadata(null) {
            BindsTwoWayByDefault = true,
            DefaultUpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.LostFocus
        });
        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }

        public static readonly DependencyProperty IsMultilineProperty = DependencyProperty.Register("IsMultiline", typeof(bool), typeof(HeaderTextBox), new UIPropertyMetadata(false));
        public bool IsMultiline
        {
            get
            {
                return (bool)GetValue(IsMultilineProperty);
            }
            set
            {
                SetValue(IsMultilineProperty, value);
                
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if(e.Property.Name == "IsMultiline")
            {
                Box.Style = Application.Current.FindResource(IsMultiline ? "MultilineTextBox" : "BasicTextBox") as Style;
            }
        }
    }
}
