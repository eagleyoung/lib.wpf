﻿using System.Windows;
using System.Windows.Controls;

namespace Lib.WPF.Controls
{
    /// <summary>
    /// Логика взаимодействия для SearchField.xaml
    /// </summary>
    public partial class SearchField : UserControl
    {
        public SearchField()
        {
            ProceedCommand = new Command(Proceed);
            InitializeComponent();
        }

        public static readonly DependencyProperty SearchTextProperty = DependencyProperty.Register("SearchText", typeof(string), typeof(SearchField), new FrameworkPropertyMetadata(null) {
            BindsTwoWayByDefault = true,
            DefaultUpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged
        });
        public string SearchText
        {
            get
            {
                return (string)GetValue(SearchTextProperty);
            }
            set
            {
                SetValue(SearchTextProperty, value);
            }
        }

        public Command ProceedCommand { get; }
        void Proceed()
        {
            SearchText = null;
        }
    }
}
