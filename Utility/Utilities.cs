﻿using Lib.WPF.Utility;

namespace Lib.WPF
{
    /// <summary>
    /// Дополнительные возможности
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// Показать окно с сообщением
        /// </summary>
        public static void ShowPopup(string text)
        {
            var window = new PopupWindow();
            window.Show(text);
        }

        /// <summary>
        /// Варианты ответа пользователя
        /// </summary>
        public enum AnswerType { None, Yes, No, Cancel }

        /// <summary>
        /// Получить булевый ответ на вопрос от пользователя
        /// </summary>
        public static bool GetBool(string text)
        {
            var answer = Show(text, false);
            return answer == AnswerType.Yes;
        }

        /// <summary>
        /// Получить ответ на вопрос от пользователя
        /// </summary>
        public static AnswerType GetAnswer(string text)
        {
            return Show(text, true);
        }

        static AnswerType Show(string text, bool extendedMode)
        {
            AnswerType answer = AnswerType.None;

            var window = new RequestWindow();
            answer = window.Get(text, extendedMode);

            return answer;
        }

        /// <summary>
        /// Получить текстовый ответ на запрос
        /// </summary>
        public static string GetText(string request)
        {
            var window = new GetTextWindow();
            var answer = window.Get(request);
            return answer;
        }
    }
}
