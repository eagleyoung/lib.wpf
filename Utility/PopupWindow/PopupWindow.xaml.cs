﻿using System;
using System.Threading;
using System.Windows;

namespace Lib.WPF.Utility
{
    /// <summary>
    /// Логика взаимодействия для PopupWindow.xaml
    /// </summary>
    public partial class PopupWindow : BaseView
    {
        /// <summary>
        /// Длительность ожидания до закрытия в миллисекундах
        /// </summary>
        int waitDuration = 5000;

        Timer closeTimer;

        public PopupWindow()
        {
            DataContext = new PopupWindowModel();
            InitializeComponent();
            closeTimer = new Timer(TimerElapsed, null, waitDuration, Timeout.Infinite);
        }

        void TimerElapsed(object state)
        {
            try
            {
                new Action(delegate {
                    Close();
                }).UIInvoke();                
            }
            catch { }            
        }

        public void Show(string text)
        {
            (Vm as PopupWindowModel).OutputText = text;
            Show();
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            var workingArea = SystemParameters.WorkArea;
            Left = workingArea.Right - Width;
            Top = workingArea.Bottom - Height;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            closeTimer.Dispose();
        }
    }
}
