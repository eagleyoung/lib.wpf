﻿namespace Lib.WPF.Utility
{
    public class PopupWindowModel : BaseViewModel
    {
        string outputText;
        /// <summary>
        /// Текст для вывода
        /// </summary>
        public string OutputText
        {
            get
            {
                return outputText;
            }
            set
            {
                if (outputText != value)
                {
                    outputText = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
