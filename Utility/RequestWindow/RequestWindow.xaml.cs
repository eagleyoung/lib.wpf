﻿namespace Lib.WPF.Utility
{
    /// <summary>
    /// Логика взаимодействия для RequestWindow.xaml
    /// </summary>
    public partial class RequestWindow : BaseView
    {
        public RequestWindow()
        {
            DataContext = new RequestWindowModel();
            InitializeComponent();
        }

        public Utilities.AnswerType Get(string text, bool extendedMode)
        {
            (Vm as RequestWindowModel).RequestText = text;
            (Vm as RequestWindowModel).ExtendedMode = extendedMode;
            ShowDialog();

            return (Vm as RequestWindowModel).Answer;
        }
    }
}
