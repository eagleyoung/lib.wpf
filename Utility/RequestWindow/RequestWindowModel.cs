﻿namespace Lib.WPF.Utility
{
    public class RequestWindowModel : BaseViewModel
    {
        public Utilities.AnswerType Answer { get; private set; } = Utilities.AnswerType.None;

        string requestText;
        /// <summary>
        /// Текст запроса
        /// </summary>
        public string RequestText
        {
            get
            {
                return requestText;
            }
            set {
                if(requestText != value)
                {
                    requestText = value;
                    OnPropertyChanged();
                }
            }
        }

        bool extendedMode;
        /// <summary>
        /// Расширенный режим (кнопка отмена)
        /// </summary>
        public bool ExtendedMode
        {
            get
            {
                return extendedMode;
            }
            set
            {
                if(extendedMode != value)
                {
                    extendedMode = value;
                    OnPropertyChanged();
                }
            }
        }

        public Command YesCommand { get; }
        void Yes()
        {
            Answer = Utilities.AnswerType.Yes;
            OnRequestClose();
        }

        public Command NoCommand { get; }
        void No()
        {
            Answer = Utilities.AnswerType.No;
            OnRequestClose();
        }        

        public Command CancelCommand { get; }
        void Cancel()
        {
            Answer = Utilities.AnswerType.Cancel;
            OnRequestClose();
        }

        public RequestWindowModel()
        {
            YesCommand = new Command(Yes);
            NoCommand = new Command(No);
            CancelCommand = new Command(Cancel);
        }
    }
}
