﻿namespace Lib.WPF.Utility
{
    /// <summary>
    /// Логика взаимодействия для GetTextWindow.xaml
    /// </summary>
    public partial class GetTextWindow : BaseView
    {
        public GetTextWindow()
        {
            DataContext = new GetTextWindowModel();
            InitializeComponent();
        }

        /// <summary>
        /// Получить текстовый ответ на запрос
        /// </summary>
        public string Get(string request)
        {
            (Vm as GetTextWindowModel).Request = request;
            ShowDialog();

            return (Vm as GetTextWindowModel).Answer;
        }
    }
}
