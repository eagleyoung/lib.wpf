﻿namespace Lib.WPF.Utility
{
    public class GetTextWindowModel : BaseViewModel
    {
        string request;
        /// <summary>
        /// Запрос
        /// </summary>
        public string Request
        {
            get
            {
                return request;
            }
            set
            {
                if(request != value)
                {
                    request = value;
                    OnPropertyChanged();
                }
            }
        }

        string answer;
        /// <summary>
        /// Ответ
        /// </summary>
        public string Answer
        {
            get
            {
                return answer;
            }
            set
            {
                if(answer != value)
                {
                    answer = value;
                    OnPropertyChanged();
                }
            }
        }

        bool answered;

        public Command OkCommand { get; }
        void Ok()
        {
            answered = true;
            OnRequestClose();
        }

        public override void OnClosed()
        {
            base.OnClosed();
            if (!answered)
            {
                Answer = null;
            }
        }

        public GetTextWindowModel()
        {
            OkCommand = new Command(Ok);
        }
    }
}
